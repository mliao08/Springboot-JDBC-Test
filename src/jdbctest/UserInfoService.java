package jdbctest;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;


@Service
public class UserInfoService {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public String create() {
		String sql = "CREATE TABLE `tb_userinfo` ( `name` varchar(255) NOT NULL,`age` varchar(255) NOT NULL,`address` varchar(255) NOT NULL) DEFAULT CHARSET=utf8;";
		
		jdbcTemplate.execute(sql);
		
		return "Success create table";
	}
	
	public String insert(UserInfo userInfo){
		String sql = "INSERT INTO tb_userinfo VALUES(?,?,?);";
		jdbcTemplate.update(sql, new PreparedStatementSetter(){

			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				// TODO Auto-generated method stub
				ps.setString(1, userInfo.getName());
				ps.setString(2, userInfo.getAge());
				ps.setString(3, userInfo.getAddress());
			}
			
		});
		
		return "Success insert table";
	}

	public String update(UserInfo userInfo) {
		return "Success update table";
	}
	
	public String drop() {
		String sql = "DROP TABLE IF EXISTS tb_userinfo;";
		jdbcTemplate.execute(sql);
		
		return "Success drop table"; 
	}
	
	public List<UserInfo> query() {
		List<UserInfo> list = null;
		
		String sql = "SELECT * FROM tb_userinfo;";
		list = jdbcTemplate.query(sql, new RowMapper<UserInfo>(){

			@Override
			public UserInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				UserInfo userInfo = new UserInfo();
				userInfo.setName(rs.getString(1));
				userInfo.setAge(rs.getString(2));
				userInfo.setAddress(rs.getString(3));
				return userInfo;
			}
		});
		
		return list;
	}
	
	public String load() {
		String sql = "LOAD DATA INFILE 'D:/test.bcp' INTO TABLE tb_userinfo FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n';";
		jdbcTemplate.execute(sql);
		return "Success load table";
	}
}
