package jdbctest;
/*Main class shouldn't be placed under default package.*/

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class JdbctestRestController {
	private static final Logger logger = LoggerFactory.getLogger(JdbctestRestController.class);
	
	@Autowired
	private UserInfoService userInfoService;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(JdbctestRestController.class, args);
	}
	
	@RequestMapping(value="/create-table",method=RequestMethod.POST)
	public String createTable() {
		return userInfoService.create();
	}
	
	@RequestMapping(value="/insert",method=RequestMethod.POST)
	public String insertTable(@RequestBody UserInfo userInfo) {
		return userInfoService.insert(userInfo);
	}
	
	@RequestMapping(value="/update",method=RequestMethod.POST)
	public String updateTable(@RequestBody UserInfo userInfo) {
		return userInfoService.update(userInfo);
	}
	
	@RequestMapping(value="/query",method=RequestMethod.POST,produces="application/json")
	public List<UserInfo> query() {
		return userInfoService.query();
	}
	
	@RequestMapping(value="/load",method=RequestMethod.POST)
	public String loadFileIntoTable() {
		return userInfoService.load();
	}
	
	@RequestMapping(value="/drop",method=RequestMethod.POST)
	public String dropTable() {
		return userInfoService.drop();
	}
}
